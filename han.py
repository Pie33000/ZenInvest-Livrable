from keras.models import Model
from keras.layers import Dense, Input, Activation, multiply, Lambda
from keras.layers import TimeDistributed, GRU, Bidirectional
from keras import backend as K
import numpy as np
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical


def han():

    # Création de la couche d'input de notre HAN (Hybrid Attention Network)
    input1 = Input(shape=(40, 200), dtype='float32')
    # Attention Layer
    dense_layer = Dense(200, activation='tanh')(input1)
    softmax_layer = Activation('softmax')(dense_layer)
    attention_mul = multiply([softmax_layer,input1])
    vec_sum = Lambda(lambda x: K.sum(x, axis=1))(attention_mul)
    pre_model = Model(input1, vec_sum)

    # Input de notre réseau de neuronne de shape (None,11,10,200)
    input2 = Input(shape=(11, 40, 200), dtype='float32')

    # LSTM bidirectionnel
    pre_lstm = TimeDistributed(pre_model)(input2)
    l_lstm = Bidirectional(GRU(100, return_sequences=True))(pre_lstm)
    post_lstm = TimeDistributed(pre_model)(l_lstm)

    # Réseaux de neuronnes denses, avec réduction de la dimension
    dense1 = Dense(100, activation='tanh')(post_lstm)
    dense2 = Dense(3, activation='tanh')(dense1)
    final = Activation('softmax')(dense2)
    final_model = Model(input2, final)
    final_model.summary()

    return final_model


def load_data(model, x_train_file, x_test_file, y_train_file, y_test_file):
    x_train = np.load(x_train_file)
    y_train = np.load(y_train_file)

    x_test = np.load(x_test_file)
    y_test = np.load(y_test_file)


    encoder = LabelEncoder()
    encoder.fit(y_train)
    encoded_Y = encoder.transform(y_train)
    y_train_end = to_categorical(encoded_Y)


    encoder2 = LabelEncoder()
    encoder.fit(y_test)
    encoded_Y2 = encoder.transform(y_test)
    y_test_end = to_categorical(encoded_Y2)
    print(y_test_end.shape)



    print("model compiling - Hierachical attention network")

    model.compile(optimizer='adam',loss='categorical_crossentropy', metrics=['accuracy'])


    print("model fitting - Hierachical attention network")

    print(x_train.shape, y_test_end.shape)

    model.fit(x_train, y_train_end, epochs=200)


    print("validation_test")

    final_x_test_predict = model.predict(x_train)



    print("Prediction de Y ", final_x_test_predict)
    print("vrai valeur Y ", y_train)

    return


if __name__ == "__main__":
    model = han()
    # il faut rajouter les fichiers à passer
    load_data(model, '', '', '', '')






