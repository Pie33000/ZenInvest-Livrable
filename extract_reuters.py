import newspaper
from newspaper import Article
from datetime import datetime
import multiprocessing as mp
import threading as tr
from multiprocessing import Pool
import time
import requests
import numpy as np
from urllib.request import urlopen
from bs4 import BeautifulSoup
import os
import time
import random


def extraction_reuters(folder,year,nb_days,nb_articles_per_day, days_list):

    """
    Scrapping des données reuters
    :param folder: répertoire où on enregistre les données
    :param year: nombre d'années à récupérer
    :param nb_days: nombre de jours
    :param nb_articles_per_day: nombre d'articles par jours
    :param days_list: nombre de jours limite
    :return:
    """

    # liste des agents pour simuler une connexion depuis un navigateur
    user_agent_list = [\
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",\
        "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",\
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",\
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",\
        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",\
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",\
        "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",\
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",\
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",\
        "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",\
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",\
        "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24"
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.167 Chrome/64.0.3282.167 Safari/537.36"
       ]

    folder = '/home/zeninvest/text_data/'
    failed_urls = folder + 'failed_urls.txt'
    nb_days = 1000
    year_str = str(year)
    folder = '/home/zeninvest/text_data/'
    start_time = time.time()
    nb_days = 1000
    nb_articles_per_day = 10000
    day_count = 0
    total = 0
    title = "http://www.reuters.com"
    journal = 'reuters/'
    year_pattern = '/resources/archive/us/' + year_str
    title = "http://www.reuters.com"
    article_pattern = 'http://www.reuters.com/article'

    for link_d in days_list:

        if day_count<nb_days:
            # On récupère les informations importantes dans l'url
            date = link_d[22:30]
            date = datetime.strptime(date, '%Y%m%d')
            date = str(date)[:10]
            elapsed_time = time.time() - start_time

            print("-" * 25)
            print('\n scrapping REUTERS for date ; {}\n'.format(date))
            print('total articles scrapped : {} \n'.format(total))
            print('elapsed_time : {}'.format(elapsed_time))

            # On créé un répoertoire avec la date récupéré dans l'url
            
            date_dir = date + '/'


            # On liste l'ensemble des articles à cette date
            day_path = os.path.join(folder, date_dir)
            if not os.path.exists(day_path):
                os.makedirs(day_path)

            journal_dir = os.path.join(day_path, journal)
            if not os.path.exists(journal_dir):
                os.makedirs(journal_dir)

            articles_count = 0
            full_url = title + link_d
            html_d = urlopen(full_url)
            bs2 = BeautifulSoup(html_d.read())
            day_count += 1
            
            l2 = [x.get('href') for x in bs2.find_all("a") if x.get('href') is not None]
            article_list = [x for x in l2 if x[:len(article_pattern)] == article_pattern]
            
            for link_a in article_list:
                
                headers = {'User-Agent': random.choice(user_agent_list)}

                try:
                    html_a = requests.get(link_a, timeout=10, headers=headers).text
                except Exception:
                    print('failed url  : {}  date : {}'.format(link_a, date))
                    f = open(failed_urls, 'a')
                    f.write(date + " " + link_a + '\n')
                    f.close()
                    pass
                
                bsa = BeautifulSoup(html_a)
                try:
                    article_title = bsa('h1')[0].text.replace("/", " ") + ' '
                except Exception:
                    article_title = bsa('p')[0].text[:80]
                article = bsa('p')
                article = [tex.text for tex in article if len(tex.text) > 80]
                text = ''.join([x for x in article])
                filename = article_title[:50] + '.txt'
                article_text = article_title + text

                file_path = os.path.join(journal_dir, filename)
                if not os.path.exists(file_path):
                    f = open(file_path, 'w')
                    f.write(article_text)
                    f.close()
                articles_count += 1
                total += 1
            
            elapsed_time = time.time() - start_time

            print("-" * 25)
            print('\n scrapping REUTERS for date ; {}\n'.format(date))
            print('total articles scrapped : {} \n'.format(total))
            print('elapsed_time : {}'.format(elapsed_time))
        else:
            break
     
      
    print('Total articles scrapped : {}'.format(total))
    return('Total articles scrapped : {}'.format(total))


def main():

    """
    Lancement du scapping
    :return:
    """
    
    folder = '/home/zeninvest/text_data/'
    failed_urls = folder + 'failed_urls'
    nb_days = 1000
    nb_articles_per_day = 10000
    year_list = [2015, 2014]
    nb_process = 20

    # On scrappe les années 2014, 2015
    for year in year_list:
        year_str = str(year)
        journal = 'reuters/'
        html_year = urlopen("http://www.reuters.com/resources/archive/us/" + year_str + ".html") # Insert your URL to extract
        # Parser de html et xml pour récupérer les liens des articles
        bs1 = BeautifulSoup(html_year.read())
        year_pattern = '/resources/archive/us/' + year_str
        title = "http://www.reuters.com"
        article_pattern = 'http://www.reuters.com/article'

        # On récupère les balises html indiquant un lien
        l = [x.get('href') for x in bs1.find_all("a")]
        days_list = [x for x in l if x[:len(year_pattern)] == year_pattern]
        

        day_repartition = list(np.array_split(days_list, nb_process))
        day_repartition = [list(x) for x in day_repartition]
        
        arg_list = [[folder, year, nb_days,nb_articles_per_day, x] for x in day_repartition]

        # On scrap une liste d'article en paraléllisant les opération
        process_list = [mp.Process(target=extraction_reuters,
                        args=arg) for arg in arg_list]
        

        
        for p in process_list:
          p.start()

        for p in process_list :
          p.join()


if __name__ == '__main__':
    main()

