from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
import os
import numpy as np


class OurDoc2Vec(object):

    """
    Class pour créer notre model d'article vectorisé
    """

    def __init__(self, dirname, model_path):

        """
        Constructeur de la classe
        :param dirname: nom du répertoire
        :param model_path: chemin de sauvegarde du model
        """

        self.dirname = dirname
        self.model_path = model_path
        self.tagged_data = []

    def prepare_data(self):

        """
        Préparer les données en créant un tableau d'article associé à un tag
        :return:
        """

        data = []
        tag = []
        i = 0

        for newspaper in os.listdir(self.dirname):
            days = os.path.join(self.dirname, newspaper)
            for day in os.listdir(days):
                day_path = os.path.join(days, day)
                for fname in os.listdir(day_path):
                    f_path = os.path.join(day_path, fname)
                    print(f_path)
                    data.append(open(f_path, 'rb').read())
                    tag.append(fname[:-5])
                    print(i)
                    i += 1

        self.tagged_data = [TaggedDocument(words=word_tokenize(str(_d.lower())), tags=[str(tag[i])]) for i, _d in
                            enumerate(data)]

        # On libère la mémoire, la quantité de données le nécessite

        data = []
        tag = []

    def train_doc2vec(self, max_epochs=15, vec_size=200, alpha=0.025):
        """
        Entrainement du doc2vec qui modélise les donneées en vecteur de dimension 200
        :param max_epochs: nombre de fois où chaque vecteur est utilisé pour mettre à jour les poids
        :param vec_size: dimension du vecteur représentant chaque article, doit être adapté en fonction de la taille du dataset
        :param alpha: taux d'apprentissage, un taux faible garantie la convergence mais elle sera plus lente voir inexistante si la fonction n'est pas convexe,
        si la valeur est trop faible il y'a un risque de ne pas pouvir sortir d'un minimum local et de ne jamais atteindre le minimum global
        :return:
        """
        model = Doc2Vec(vector_size=vec_size, alpha=alpha, min_alpha=0.025, min_count=5,
                        dm=1, workers=30)

        model.build_vocab(self.tagged_data)


        for epoch in range(max_epochs):
            print('iteration {0}'.format(epoch))
            model.train(self.tagged_data,
                        total_examples=model.corpus_count,
                        epochs=model.iter)
            # on diminue le taux d'apprentissage à chaque itération
            model.alpha -= 0.0002
            # on initialise le nouveau alpha
            model.min_alpha = model.alpha

        model.save(self.model_path)
        print("Model sauvegardé")

    def clean_train_model(self):

        """
        Permet de passer le modèle en production, attention une mauvaise utilisation entraine la perte du train
        :return:
        """

        model = Doc2Vec.load(self.model_path)
        # Si mal configuré supprime tous les vecteurs
        model.delete_temporary_training_data(keep_doctags_vectors=True, keep_inference=True)

    def test_doc2vec(self):

        """
        Teste de notre modèle
        :return:

        """
        model = Doc2Vec.load(self.model_path)

        model.docvecs.doctags

        # test_data = word_tokenize("Odorizzi".lower())
        # test_data2 = word_tokenize("Page".lower())

        # v1 = model.infer_vector(test_data)
        # v2 = model.infer_vector(test_data2)

        # to find most similar doc using tags
        vector = model.docvecs['0000_40']
        print(type(vector))
        print("Vector of document:", vector)

    def readFile(self):

        """
        Lire les fichiers créé par le Doc2Vec
        :return:
        """

        model = Doc2Vec.load(self.model_path)

        print(type(model.docvecs.doctags))

        file = np.load('/Users/rugerypierrick/PycharmProjects/doc2vec/d2v.model.docvecs.vectors_docs.npy')

        fil2 = np.load('/Users/rugerypierrick/PycharmProjects/doc2vec/d2v.model.trainables.syn1neg.npy')

        file3 = np.load('/Users/rugerypierrick/PycharmProjects/doc2vec/d2v.model.wv.vectors.npy')


        print(file3)


if __name__ == '__main__':
    model = OurDoc2Vec("text_data", "d2v.model")
    #model.test_doc2vec()
    model.readFile()

