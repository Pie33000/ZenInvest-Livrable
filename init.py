import zipfile as zp
from subprocess import call


def manage_date(path):

    call(["pip", "install", "gensim"])
    call(["python3","-mpip", "install", "-U", "pip"])
    call(["python3", "-mpip", "install", "-U", "pip"])


    with zp.ZipFile(path) as myzip:
        myzip.extractall(path='./')
    print("Extraction des données réussie")


if __name__ == '__main__':
    manage_date('./datas_y_train.zip')
