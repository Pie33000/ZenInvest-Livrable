import numpy
import matplotlib.pyplot as plt
import pandas as pd
import math
import numpy as np

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn import linear_model

"""
    Régression linéaire pour prédire le cours de la bourse en fonction des cours précédents
"""

# fix random seed for reproducibility
numpy.random.seed(7)

# load the dataset
df = pd.read_csv('./apple_since_2014.csv',usecols=[1])
x=df.values
x=x.astype("float32")

# split into train and test sets
train_size = int(len(x) * 0.67)
test_size = len(x) - train_size
train, test = x[0:train_size,:], x[train_size:len(x),:]
print(len(train), len(test))

linear_test_scores = {}
linear_train_scores = {}


# convert an array of values into a dataset matrix
def create_dataset2(dataset, look_back=1):
    dataX, dataY, yvalue = [], [], []
    for i in range(len(dataset) - look_back - 1):
        a = np.array([x + 1 for x in range(look_back)])
        dataX.append(a)
        dataY.append(dataset[i:(i + look_back), 0])
        yvalue.append(dataset[i + look_back + 1, 0])

    return np.array(dataX), np.array(dataY), np.array(yvalue)


def linear_training(look_back):
    testX, testY, yvalue = create_dataset2(test, look_back)
    # for i in range(len(testX)):
    # testX[i]=[np.arange(1,look_back+1),testX[i]]
    # testY[i]=[look_back+1,testY[i]]

    model_linear = linear_model.LinearRegression()
    # reshaping

    print("-" * 25)
    print("training for look_back= " + str(look_back))
    testPredict = np.zeros(len(testX) - look_back - 1)
    for i in range(len(testX) - look_back - 1):
        testX2 = testX[i].reshape(-1, 1)
        testY2 = testY[i].reshape(-1, 1)
        model_linear.fit(testX2, testY2)
        testPredict[i] = model_linear.predict(look_back + 1)

    # calculate root mean squared error
    testScore = math.sqrt(mean_squared_error(yvalue[:-look_back - 1], testPredict))

    linear_test_scores[look_back] = testScore
    return testPredict, testY, testX, yvalue


for i in range(4, 16):
    linear_training(i)


linear_test_scores_list = sorted(linear_test_scores.items()) # sorted by key, return a list of tuples

x, y = zip(*linear_test_scores_list) # unpack a list of pairs into two tuples

plt.plot(x, y, linewidth=3)
plt.title(" RMSE en fonction de la taille de l'input")
plt.ylabel("RMSE")
plt.xlabel("nombre de jours en input")
plt.grid(True)
plt.show()

look_back=5
testPredict,testY,testX,yvalue=linear_training(look_back)
# shift train predictions for plotting
testPredictPlot = np.zeros(np.shape(testPredict)[0])
testPredictPlot[:] = np.nan
testPredictPlot[:] = testPredict

# plot baseline and predictions
plt.plot(testPredictPlot, 'g')
plt.plot(yvalue,'r')
plt.show()