import pickle
import numpy as np
from gensim.models.doc2vec import Doc2Vec
from sklearn.model_selection import train_test_split
import os
import multiprocessing as mp


def create_x_y(name, model, firm_article, firm_stock, k_max=40, test_size=0.33, window=10):
    """
    Fonction pour créer le dataset
    :param name: nom de l'entreprise qu'on traite
    :param model: model doc2vec permet de renvoyer les vecteur associé à un document
    :param firm_article: liste des articles associé à l'entreprise name
    :param firm_stock: liste des cours de la bourse associé à l'entreprise name
    :param k_max: nombre d'article max par jour que le modèle sélectionne
    :param test_size: séparation du dataset en deux jeux de données train et test
    :param window: nombre de jour précédent pris en compte
    :return: x_train, x_test, y_train, y_test
    """

    # Ouverture du fichier contenant les articles associé à l'entreprise
    with open(firm_article, 'rb') as dict_article:
        dico_article = pickle.load(dict_article)
    # Ouverture du fichier contenant les cours de la bourse associé à l'entreprise
    with open(firm_stock, 'rb') as dict_stock:
        dico_stock = pickle.load(dict_stock)

    # Vérifier que la liste d'articles et de cours de la bourse soit non vide
    if (len(dico_article) > 0) and (len(dico_stock) > 0):

        # i=nb de jour total ou on a un article sur cette entreprise
        # j=nb de jour en arrière pris pour pour le train = window
        # k= nb d'articles par jour
        # l= dim_vec=200, dimension de l'article vectorisé

        # création de notre matrice data
        data = np.zeros((1, 11, k_max, 200), dtype='float32')

        y = []
        dates = []

        for i in range(int(1095)):
            # booléan pour savoir si un article a été publié ce jour la
            to_add = False
            # on veut prédire le jour i+1. On vérifie si ce jour est dans les clefs de dico_stock
            next_day_key = str(i + 1).zfill(4)
            if next_day_key in dico_stock:
                y_i = int(dico_stock[next_day_key])
                # nouvelle ligne à ajouter potentiellement
                new_row = np.zeros((1, 11, k_max, 200), dtype='float32')
                for j in range(11):
                    # on regarde de i à i-k
                    day_key = str(i - j).zfill(4)
                    if (day_key in dico_article):
                        list_article = dico_article[day_key]
                        to_add = True
                        # k= clef, x=valeur=liste des ID d'articles du jour
                        for k in range(k_max):
                            if k < len(dico_article[day_key]):
                                article_id = list_article[k]
                                vector = model.docvecs[article_id]
                                new_row[0, j, k, :] = vector
                            else:
                                new_row[0, j, k, :] = np.zeros(200)

                if to_add:
                    # on ajoute la ligne
                    data = np.vstack([data, new_row])
                    y.append(y_i)
                    dates.append(next_day_key)

        y_vec = np.asarray(y)
        x_mat = np.delete(data, (0), axis=0)  # on dégage la 1ere ligne qui n'a que des zéros
        x_train, x_test, y_train, y_test = train_test_split(
            x_mat, y_vec, test_size=test_size, random_state=42, shuffle=False)

        y_train_size = y_train.shape[0]
        dates_test = dates[y_train_size:]

        with open('./dates/dates' + name + '.pkl', 'wb') as handle:
            pickle.dump(dates_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

        return (x_train, x_test, y_train, y_test)
    else:
        print("Error File")
        return [], [], [], []


def open_pickle(name_file):
    with open(name_file, 'rb') as handle:
        b = pickle.load(handle)
    print(b)


def open_numpy():
    x1_test = np.load('./3M_x_train.npy')
    x2_test = np.load('./Abbott_x_train.npy')
    print(x1_test.shape)
    print(x2_test.shape)


def create_dataset(stockfile_list, articlefile_list):
    """
    function qui permet de lancer la création des datasets
    :param stockfile_list: liste des fichiers de cours de la bourses pour n entreprises
    :param articlefile_list: liste des fichiers d'articles pour n entreprises
    :return:
    """

    # Vérifier qu'on a le même nombre d'entreprise
    assert len(stockfile_list) == len(articlefile_list)

    # Chargement du model doc2vec
    model = Doc2Vec.load('./d2v.model')


    for i, j in enumerate(stockfile_list):
        # stocker le nom de l'entreprise en supprimant les extensions
        name = articlefile_list[i][:-8]

        print('./pickle_article/' + articlefile_list[i])
        print('./pickle/' + stockfile_list[i])
        print(name)

        x_train, x_test, y_train, y_test = create_x_y(name, model, './pickle_article/' + articlefile_list[i],
                                                      './pickle/' + stockfile_list[i], 10, 0.33, 10)

        if len(x_train) > 0:

            # Encodage du y_test pour avoir une dimension 3
            y_test_encode_start = list()

            for trend in y_test:
                new_value = trend + 1
                code = [0 for _ in range(3)]
                code[new_value] = 1
                y_test_encode_start.append(code)
            y_test_encode_end = np.asarray(y_test_encode_start)
            # Fin de l'encodage

            # Création des fichiers numpy
            np.save('./x_train/' + name + '_x_train.npy', x_train)
            np.save('./x_test/' + name + '_x_test.npy', x_test)

            np.save('./y_train/' + name + '_y_train.npy', y_train)
            np.save('./y_test/' + name + '_y_test.npy', y_test_encode_end)

        # Compteur d'tération
        nb_iteration = len(stockfile_list) - i
        print("Nb itérations restantes :", nb_iteration)


'''stock_directory = './pickle'
article_directory = './pickle_article'
stockfile_list = os.listdir(stock_directory)
articlefile_list = os.listdir(article_directory)
nb_process = 8

# trie des listes avant le split
stockfile_list.sort()
articlefile_list.sort()

#split des listes des fichiers des cours de bourses et d'articles
stock_repartition = list(np.array_split(stockfile_list, nb_process))
article_repartition = list(np.array_split(articlefile_list, nb_process))

stock_repartition = [list(x) for x in stock_repartition]
article_repartition = [list(x) for x in article_repartition]

# création d'un tableau à passer en argument
arg_list = []
for i in range(len(stock_repartition)):
    stock_arg = stock_repartition[i]
    article_arg = article_repartition[i]
    arg = [stock_arg, article_arg]
    arg_list.append(arg)

# Lancement du multi thread
process_list = [mp.Process(target=create_dataset, args=arg) for arg in arg_list]

for p in process_list:
    p.start()

for p in process_list:
    p.join()'''


if __name__ == "__main__":
    #create_dataset('./pickle', './pickle_article')
    open_pickle('/Users/rugerypierrick/ZenIvest/pickle/3M .pkl')








