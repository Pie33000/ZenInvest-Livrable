import numpy
import matplotlib.pyplot as plt
import pandas as pd
import math
import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn import linear_model

"""
    LSTM qui prévoit le cours de la bourse en fonction des cours précédents
"""

# fix random seed for reproducibility
numpy.random.seed(7)


# load the dataset
df = pd.read_csv('/Users/rugerypierrick/ZenIvest/apple_since_2014.csv', usecols=[1])
x = df.values
x = x.astype("float32")

# normalize the dataset
scaler = MinMaxScaler(feature_range=(0, 1))
x = scaler.fit_transform(x)

# split into train and test sets
train_size = int(len(x) * 0.67)
test_size = len(x) - train_size
train, test = x[0:train_size, :], x[train_size:len(x), :]
print(len(train), len(test))



def create_dataset(dataset, look_back=1):
    """
    Create dataset
    :param dataset:
    :param look_back: fenetre valeur précédente
    :return:
    """

    dataX, dataY = [], []
    for i in range(len(dataset)-look_back-1):
        a = dataset[i:(i+look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return np.array(dataX), np.array(dataY)

# reshape into X=t and Y=t+1
look_back = 7
trainX, trainY = create_dataset(train, look_back)
testX, testY = create_dataset(test, look_back)
# reshape input to be [samples, time steps, features]
trainX = numpy.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
testX = numpy.reshape(testX, (testX.shape[0], 1, testX.shape[1]))

# create and fit the LSTM network

model = Sequential()
model.add(LSTM(10, input_shape=(1, look_back)))
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(trainX, trainY, epochs=4, batch_size=1, verbose=1)

# make predictions
trainPredict = model.predict(trainX)
testPredict = model.predict(testX)
# invert predictions
trainPredict = scaler.inverse_transform(trainPredict)
trainY = scaler.inverse_transform([trainY])
testPredict = scaler.inverse_transform(testPredict)
testY = scaler.inverse_transform([testY])
# calculate root mean squared error
trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:,0]))
print('Train Score: %.2f RMSE' % (trainScore))
testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:,0]))
print('Test Score: %.2f RMSE' % (testScore))

# shift train predictions for plotting
trainPredictPlot = numpy.empty_like(x)
trainPredictPlot[:, :] = numpy.nan
trainPredictPlot[look_back:len(trainPredict)+look_back, :] = trainPredict
# shift test predictions for plotting
testPredictPlot = numpy.empty_like(x)
testPredictPlot[:, :] = numpy.nan
testPredictPlot[len(trainPredict)+(look_back*2)+1:len(x)-1, :] = testPredict
# plot baseline and predictions
plt.plot(scaler.inverse_transform(x))
plt.plot(trainPredictPlot)
plt.plot(testPredictPlot)
plt.show()

test_scores = {}
train_scores = {}


def lstm_training(look_back):
    trainX, trainY = create_dataset(train, look_back)
    testX, testY = create_dataset(test, look_back)
    # reshaping
    trainX = numpy.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
    testX = numpy.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
    print("-" * 25)
    print("training for look_back= " + str(look_back))
    model = Sequential()
    model.add(LSTM(4, input_shape=(1, look_back)))
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    model.fit(trainX, trainY, epochs=5, batch_size=1, verbose=1)

    # make predictions
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)
    # invert predictions
    trainPredict = scaler.inverse_transform(trainPredict)
    trainY = scaler.inverse_transform([trainY])
    testPredict = scaler.inverse_transform(testPredict)
    testY = scaler.inverse_transform([testY])
    # calculate root mean squared error
    trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:, 0]))
    testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:, 0]))

    train_scores[look_back] = trainScore
    test_scores[look_back] = testScore


for i in range(1, 16):
    lstm_training(i)

test_scores_list = sorted(test_scores.items()) # sorted by key, return a list of tuples

x, y = zip(*test_scores_list) # unpack a list of pairs into two tuples

plt.plot(x, y, linewidth=3)
plt.title(" RMSE en fonction de la taille de l'input")
plt.ylabel("RMSE")
plt.xlabel("nombre de jours en input")
plt.grid(True)
plt.show()
