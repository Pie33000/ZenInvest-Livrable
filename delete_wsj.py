import shutil
import os


def delete_wsj(folder):
    for x in os.listdir(folder):
        path_x = os.path.join(folder, x)
        wsj_folder = os.path.join(path_x, 'wsj/')
        print(wsj_folder)
        if os.path.isdir(wsj_folder):

            shutil.rmtree(wsj_folder, ignore_errors=True)


if __name__ == '__main__':
    folder = '/home/zeninvest/text_data/'
    delete_wsj(folder)